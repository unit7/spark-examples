package com.breezzo.spark.wordcount

import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by breezzo on 15.01.17.
 */
object WordCount {
  def main(args: Array[String]) {
    val inputFile = args(0)
    val outputFile = args(1)

    val conf = new SparkConf().setAppName("wordCount")
    val sc = new SparkContext(conf)

    val lines = sc.textFile(inputFile)

    val counts =
      lines.flatMap(line => line.split(" "))
          .map(word => (word, 1))
          .reduceByKey((f, s) => f + s)

    counts.saveAsTextFile(outputFile)
  }
}
