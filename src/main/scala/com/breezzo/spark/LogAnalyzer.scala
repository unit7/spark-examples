package com.breezzo.spark

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.regex.Pattern

import org.apache.spark._

/**
 * Created by breezzo on 16.01.17.
 */
object LogAnalyzer {
  val EVENT_REGEX =
    Pattern.compile("(\\d{4}-\\d{2}-\\d{2}){1}.*(INFO|WARN|ERROR|DEBUG|TRACE|FATAL){1}.*", Pattern.CASE_INSENSITIVE)

  def main(args: Array[String]) {
    val inputFile = args(0)
    val outputFile = args(1)
    
    val sparkConf = new SparkConf().setAppName("LogAnalyzer")
    val sc = new SparkContext(sparkConf)

    sc.textFile(inputFile)
          .map(EVENT_REGEX.matcher)
          .filter(m => m.matches())
          .map(m => (parse(m.group(1), m.group(2)), 1))
          .reduceByKey((f, s) => f + s)
          .map(v => (v._1._1, v._1._2, v._2))
          .saveAsTextFile(outputFile)
  }

  def parse(dateStr: String, levelStr: String): (LocalDate, String) = {
      val date = LocalDate.parse(dateStr, DateTimeFormatter.ofPattern("yyyy-MM-dd"))
      (date, levelStr.toUpperCase)
  }
}
